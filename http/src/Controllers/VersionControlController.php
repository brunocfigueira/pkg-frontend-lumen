<?php

namespace Pkgfigueira\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Pkgfigueira\Backend\Services\Rules\VersionControlService;


class VersionControlController extends Controller
{
    private $versionControlService;
    public function __construct(VersionControlService $versionControlService)
    {
        $this->versionControlService = $versionControlService;
    }
   public function displayAll(){       
       return response()->json([
           'data'=>$this->versionControlService->displayAll(),
        ],200);
   }
   public function displayAllApi(){       
       return response()->json([
           'data'=>$this->versionControlService->displayAll(),
        ],200);
   }
}
