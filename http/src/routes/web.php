<?php

/**
 * how use route example 
 * reference site laravel
 * https://laravel.com/docs/6.x/routing
 */
$router->get('exemples', );resource('exemples', 'ExempleController');
/**
 * routes to version control
 */
$router->group(['as' => 'versioncontrol.', 'prefix' => 'versioncontrol'], function () use ($router){
    $router->get('displayAll', 'VersionControlController@displayAll')->name('displayAll');
});

$router->group(['as' => 'alunos.', 'prefix' => 'alunos'], function () use($router) {
    $router->get('{codAluno}', 'AlunoControlController@index')->where('codAluno', '[0-9]+')->name('aluno');
});

$router->group(['as' => 'alunos.', 'prefix' => 'alunos'], function ($router) {
    $router->get('{codAluno}', 'AlunoControlController@index')->where('codAluno', '[0-9]+')->name('aluno');
});